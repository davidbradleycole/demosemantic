# CosmicDBSemantic Demo Project

## Installation
```
python manage.py migrate
python manage.py createsuperuser
python manage.py collectstatic
python manage.py sitetreeload demo_site/treedump.json
python manage.py runserver
```