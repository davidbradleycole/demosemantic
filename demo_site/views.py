from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse

from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet

from django_tables2 import SingleTableView

from cosmicdb.views import CosmicSaveErrorDialogsMixin, CosmicAutocomplete
from cosmicdb.forms import CosmicFormsetHelper

from demo_site.models import Customer, Invoice, InvoiceItem
from demo_site.tables import CustomerTable, InvoiceTable
from demo_site.forms import CustomerForm, CustomerFormWithDelete, InvoiceForm, InvoiceItemForm


class CustomerList(SingleTableView):
    template_name = 'cosmicdb/base_table.html'
    model = Customer
    table_class = CustomerTable
    table_pagination = {
        'per_page': 10
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['add_url'] = reverse('customer_new')
        context['add_label'] = 'Add Customer'
        return context


class CustomerNew(CosmicSaveErrorDialogsMixin, CreateView):
    template_name = 'cosmicdb/base_form.html'
    model = Customer
    form_class = CustomerForm


class CustomerEdit(CosmicSaveErrorDialogsMixin, UpdateView):
    template_name = 'cosmicdb/base_form.html'
    model = Customer
    form_class = CustomerFormWithDelete


class CustomerDelete(DeleteView):
    template_name = 'cosmicdb/base_form_delete.html'
    model = Customer

    def get_success_url(self):
        return reverse('customers')


class CustomerAutocomplete(CosmicAutocomplete):
    model = Customer


class InvoiceList(SingleTableView):
    template_name = 'cosmicdb/base_table.html'
    model = Invoice
    table_class = InvoiceTable
    table_pagination = {
        'per_page': 10
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['add_url'] = reverse('invoice_new')
        context['add_label'] = 'Add Invoice'
        return context


class InvoiceItemInline(InlineFormSet):
    model = InvoiceItem
    form_class = InvoiceItemForm

    def get_factory_kwargs(self):
        kwargs = super(InvoiceItemInline, self).get_factory_kwargs()
        kwargs['extra'] = 0
        kwargs['min_num'] = 1
        return kwargs


class InvoiceNew(CosmicSaveErrorDialogsMixin, CreateWithInlinesView):
    template_name = 'cosmicdb/base_form_with_inline.html'
    model = Invoice
    form_class = InvoiceForm
    inlines = [InvoiceItemInline]

    def get_context_data(self, **kwargs):
        data = super(InvoiceNew, self).get_context_data(**kwargs)
        data['formset_helper'] = CosmicFormsetHelper()
        data['formset_labels'] = {'invoiceitem_set': 'Items'}
        return data


class InvoiceEdit(CosmicSaveErrorDialogsMixin, UpdateWithInlinesView):
    template_name = 'cosmicdb/base_form_with_inline.html'
    model = Invoice
    form_class = InvoiceForm
    inlines = [InvoiceItemInline]

    def get_context_data(self, **kwargs):
        data = super(InvoiceEdit, self).get_context_data(**kwargs)
        data['formset_helper'] = CosmicFormsetHelper()
        data['formset_labels'] = {'invoiceitem_set': 'Items'}
        data['delete_button'] = True
        return data


class InvoiceDelete(DeleteView):
    template_name = 'cosmicdb/base_form_delete.html'
    model = Invoice

    def get_success_url(self):
        return reverse('invoices')

