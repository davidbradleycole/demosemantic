from django import forms
from django.urls import reverse

from crispy_forms.layout import Button

from cosmicdb.forms import CosmicFormHelper
from cosmicdb.widgets import CosmicSelectAutocomplete

from demo_site.models import Customer, Invoice, InvoiceItem


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = (
            'code',
            'name',
            'phone',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = CosmicFormHelper(self)


class CustomerFormWithDelete(CustomerForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.add_input(Button('delete', 'Delete', onclick="window.location.href = 'delete';", css_class='ui button large red'))


class InvoiceForm(forms.ModelForm):
    total = forms.DecimalField(required=False)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'customer',
        )
        widgets = {
            'customer': CosmicSelectAutocomplete(url=lambda: reverse('customer_autocomplete'))
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['total'].widget.attrs = {'value': self.instance.total(), 'disabled': 'disabled'}
        self.helper = CosmicFormHelper(self, save_button=False)
        self.helper.form_tag = False


class InvoiceItemForm(forms.ModelForm):
    total = forms.DecimalField(required=False)

    class Meta:
        model = InvoiceItem
        fields = (
            'description',
            'amount',
            'tax',
        )

    def __init__(self, *args, **kwargs):
        super(InvoiceItemForm, self).__init__(*args, **kwargs)
        self.fields['total'].widget.attrs = {'value': self.instance.total(), 'disabled': 'disabled'}
