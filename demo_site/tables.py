from cosmicdb.tables import CosmicModelTable

from demo_site.models import Customer, Invoice


class CustomerTable(CosmicModelTable):
    class Meta:
        model = Customer


class InvoiceTable(CosmicModelTable):
    class Meta:
        model = Invoice