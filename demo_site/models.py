from django.db import models
from django.urls import reverse


class Customer(models.Model):
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)

    def get_absolute_url(self):
        return reverse('customer_edit', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class Invoice(models.Model):
    date = models.DateField()
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    def total(self):
        total = 0
        for item in self.invoiceitem_set.all():
            total += item.total()
        return total

    def get_absolute_url(self):
        return reverse('invoice_edit', kwargs={'pk': self.pk})

    def __str__(self):
        return '%s - %s - %s' % (self.date, self.customer, self.total())


class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    tax = models.DecimalField(max_digits=11, decimal_places=2)

    def total(self):
        amount = self.amount or 0
        tax = self.tax or 0
        return amount + tax

