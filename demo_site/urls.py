from django.urls import path
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static

from demo_site.views import CustomerList, CustomerNew, CustomerEdit, CustomerDelete, CustomerAutocomplete, \
    InvoiceList, InvoiceNew, InvoiceEdit, InvoiceDelete


urlpatterns = [
    path('customers/', login_required(CustomerList.as_view()), name='customers'),
    path('customer/new/', login_required(CustomerNew.as_view()), name='customer_new'),
    path('customer/<int:pk>/', login_required(CustomerEdit.as_view()), name='customer_edit'),
    path('customer/<int:pk>/delete/', login_required(CustomerDelete.as_view()), name='customer_delete'),
    path('customer_autocomplete/', login_required(CustomerAutocomplete.as_view()), name='customer_autocomplete'),
    path('invoices/', login_required(InvoiceList.as_view()), name='invoices'),
    path('invoice/new/', login_required(InvoiceNew.as_view()), name='invoice_new'),
    path('invoice/<int:pk>/', login_required(InvoiceEdit.as_view()), name='invoice_edit'),
    path('invoice/<int:pk>/delete/', login_required(InvoiceDelete.as_view()), name='invoice_delete'),
]